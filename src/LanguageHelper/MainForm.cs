﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace LanguageHelper
{
	public partial class MainForm : Form
	{
		/// <summary>
		/// A list of strings that represent each line of data in the master langauge file.
		/// The master language file is used as a baseline reference to determine what translations are needed.
		/// </summary>
		public Dictionary<String, String> master;

		/// <summary>
		/// A list of strings that represent each line of data in the slave language file.
		/// The slave language file is the one being edited and updated, based on the requirements of the master language file.
		/// </summary>
		public Dictionary<String, String> slave;

		/// <summary>
		/// A list of keys from the master dictionary that are not present in the slave equivalent. These are essentially
		/// missing translatons that need to be added for the slave file to be up to date.
		/// </summary>
		public List<String> slaveMissing;

		/// <summary>
		/// A list of strings that the slave list contains, that are not present on the master list. These translations
		/// are considered redundant and can be safely removed.
		/// </summary>
		public Dictionary<String, String> slaveRedundant;


		/// <summary>
		/// Constructor for the form.
		/// </summary>
		public MainForm()
		{
			this.InitializeComponent();

			master = new Dictionary<String, String>();
			slave = new Dictionary<String, String>();
			slaveMissing = new List<String>();
			slaveRedundant = new Dictionary<String, String>();
		}


		/// <summary>
		/// Called when the 'Master' button is clicked. Allow the user to choose the file path for the master language file.
		/// </summary>
		private void ButtonChooseMaster_Click(Object sender, EventArgs e)
		{
			if (OpenFileDialog.ShowDialog() == DialogResult.OK)
			{
				TextBoxMaster.Text = OpenFileDialog.FileName;
				loadData(OpenFileDialog.FileName, ref master);
			}
		}

		/// <summary>
		/// Called when the 'Slave' button is clicked. Allow the user to choose the file path for the slave language file.
		/// </summary>
		private void ButtonChooseSlave_Click(Object sender, EventArgs e)
		{
			if (OpenFileDialog.ShowDialog() == DialogResult.OK)
			{
				TextBoxSlave.Text = OpenFileDialog.FileName;
				loadData(OpenFileDialog.FileName, ref slave);
			}

			refreshTables();
		}


		/// <summary>
		/// Called to load language file data into the specified String list.
		/// </summary>
		/// <param name="path">The full file path of the language file to laod from.</param>
		/// <param name="list">A reference of the String list to dump the data into.</param>
		public void loadData(String path, ref Dictionary<String, String> list)
		{
			list.Clear();

			Boolean loading = true;
			String[] rawData;

			while (loading)
			{
				try
				{
					rawData = File.ReadAllLines(path);
					list.Clear();

					foreach (String rawLine in rawData)
					{
						rawLine.Trim();

						// Don't import blank lines.
						if (rawLine.Length <= 0)
						{
							continue;
						}

						// Don't import comments.
						if (rawLine[0] == '#')
						{
							continue;
						}

						// Don't import broken lines.
						if (!rawLine.Contains("="))
						{
							continue;
						}

						String[] splitted = rawLine.Split("=", 2);
						if (splitted[1].Length <= 0)
						{
							list.Add(splitted[0], "");
						}
						else
						{
							list.Add(splitted[0], splitted[1]);
						}
					}

					loading = false;
					break;
				}
				catch (Exception ex)
				{
					DialogResult result = MessageBox.Show(
						null,
						"Couldn't read data from the specified path.\n" + ex.Message,
						this.Text,
						MessageBoxButtons.RetryCancel,
						MessageBoxIcon.Error,
						MessageBoxDefaultButton.Button2);

					switch (result)
					{
						case DialogResult.Retry:
							break;

						case DialogResult.Cancel:
						default:
							loading = false;
							slave.Clear();
							break;
					}
				}
			}

			refreshStatus();
		}


		/// <summary>
		/// Called to refresh the status bar on the right side of the main form. This should be done any time the state of the master/slave string lists are changed.
		/// </summary>
		public void refreshStatus()
		{
			LabelMasterElementsCount.Text = "";
			LabelSlaveElementsCount.Text = "";
			LabelSlaveMissingCount.Text = "";
			LabelSlaveRedundantCount.Text = "";
			LabelMasterElementsCount.BackColor = Color.White;
			LabelSlaveElementsCount.BackColor = Color.White;
			LabelSlaveMissingCount.BackColor = Color.White;
			LabelSlaveRedundantCount.BackColor = Color.White;
			ProgressBar.Value = 0;

			slaveMissing.Clear();
			slaveRedundant.Clear();

			if (master.Count != 0)
			{
				LabelMasterElementsCount.Text = master.Count.ToString();
			}

			{
				LabelSlaveElementsCount.Text = slave.Count.ToString();

				int missing = 0;
				int redundant = 0;

				foreach (String masterKey in master.Keys)
				{
					if (!slave.ContainsKey(masterKey))
					{
						missing++;
						slaveMissing.Add(masterKey);
					}
				}

				foreach (String slaveKey in slave.Keys)
				{
					if (!master.ContainsKey(slaveKey))
					{
						redundant++;

						String slaveValue;
						if (slave.TryGetValue(slaveKey, out slaveValue))
						{
							slaveRedundant.Add(slaveKey, slaveValue);
						}
					}
				}

				LabelSlaveMissingCount.Text = missing.ToString();
				LabelSlaveRedundantCount.Text = redundant.ToString();

				if (missing != 0)
				{
					LabelSlaveMissingCount.BackColor = Color.Khaki;
				}

				if (redundant != 0)
				{
					LabelSlaveRedundantCount.BackColor = Color.Khaki;
				}
			}

			float linesTotal = (float)master.Keys.Count;
			float linesSoFar = (float)slave.Keys.Count;
			int progress = (int)((linesSoFar / linesTotal) * 100.0F);

			if(progress < 0)
				progress = 0;
			if(progress > 100)
				progress = 100;

			ProgressBar.Value = progress;
		}


		/// <summary>
		/// Called to refresh the data inside the list view tables. The data will be retrieved from the current slave dictionary.
		/// </summary>
		public void refreshTables()
		{
			// Remember the previous indices so we can scroll back to them.
			int currentOldIndex = -1;
			int missingOldIndex = -1;
			int redundantOldIndex = -1;

			if (ListViewCurrent.SelectedIndices.Count >= 1)
				currentOldIndex = ListViewCurrent.SelectedIndices[0];
			if (ListViewMissing.SelectedIndices.Count >= 1)
				missingOldIndex = ListViewMissing.SelectedIndices[0];
			if (ListViewRedundant.SelectedIndices.Count >= 1)
				redundantOldIndex = ListViewRedundant.SelectedIndices[0];


			// Clear lists beforehand.
			ListViewCurrent.Items.Clear();
			ListViewMissing.Items.Clear();
			ListViewRedundant.Items.Clear();

			// Reused values.
			ListViewItem item;
			String slaveValue;
			String masterValue;

			// Update current list.
			foreach (String slaveKey in slave.Keys)
			{
				if (!slave.TryGetValue(slaveKey, out slaveValue))
				{
					continue;
				}

				if (!master.TryGetValue(slaveKey, out masterValue))
				{
					continue;
				}

				item = new ListViewItem(new String[] { slaveKey, masterValue, slaveValue });
				ListViewCurrent.Items.Add(item);
			}
			if (currentOldIndex >= 0 && currentOldIndex < ListViewCurrent.Items.Count)
			{
				ListViewCurrent.Items[currentOldIndex].Selected = true;
				ListViewCurrent.FocusedItem = ListViewCurrent.Items[currentOldIndex];
				ListViewCurrent.EnsureVisible(currentOldIndex);
			}

			// Update missing list.
			foreach (String missingKey in slaveMissing)
			{
				if (!master.TryGetValue(missingKey, out masterValue))
				{
					continue;
				}

				item = new ListViewItem(new String[] { missingKey, masterValue });
				ListViewMissing.Items.Add(item);
			}
			if (missingOldIndex >= 0 && missingOldIndex < ListViewMissing.Items.Count)
			{
				ListViewMissing.Items[missingOldIndex].Selected = true;
				ListViewMissing.FocusedItem = ListViewMissing.Items[missingOldIndex];
				ListViewMissing.EnsureVisible(missingOldIndex);
			}

			// Update redundant list.
			foreach (String redundantKey in slaveRedundant.Keys)
			{
				if (!slave.TryGetValue(redundantKey, out slaveValue))
				{
					continue;
				}

				item = new ListViewItem(new String[] { redundantKey, slaveValue });
				ListViewRedundant.Items.Add(item);
			}
			if (redundantOldIndex >= 0 && redundantOldIndex < ListViewRedundant.Items.Count)
			{
				ListViewRedundant.Items[redundantOldIndex].Selected = true;
				ListViewRedundant.FocusedItem = ListViewRedundant.Items[redundantOldIndex];
				ListViewRedundant.EnsureVisible(redundantOldIndex);
			}
		}


		/// <summary>
		/// Called when the 'Refresh' button is pressed. Here, manually trigger the status refresh method. Otherwise status refreshing occurs automatically when files are loaded or changed.
		/// </summary>
		private void ButtonRefresh_Click(Object sender, EventArgs e)
		{
			refreshStatus();
			refreshTables();
		}


		/// <summary>
		/// Called when an element on the current items list is double clicked.
		/// Open a window allowing the user to change the value, and then assign the new value afterwards.
		/// </summary>
		private void ListViewCurrent_DoubleClick(Object sender, EventArgs e)
		{
			if (ListViewCurrent.SelectedIndices.Count <= 0)
			{
				return;
			}

			int selectedIndex = ListViewCurrent.SelectedIndices[0];
			ListViewItem currentItem = ListViewCurrent.SelectedItems[0];
			ChangeCurrentForm form = new ChangeCurrentForm(currentItem.SubItems[0].Text, currentItem.SubItems[1].Text, currentItem.SubItems[2].Text);
			DialogResult result = form.ShowDialog(this);

			switch (result)
			{
				case DialogResult.Cancel:
					break;

				case DialogResult.OK:
					if (slave.ContainsKey(currentItem.SubItems[0].Text))
					{
						slave[currentItem.SubItems[0].Text] = form.newValue;
						refreshTables();
					}
					break;
			}
		}


		/// <summary>
		/// Called when an element on the missing items list is double clicked.
		/// Open a window allowing the user to enter in a new translation, and add it to the current list.
		/// </summary>
		private void ListViewMissing_DoubleClick(Object sender, EventArgs e)
		{
			if (ListViewMissing.SelectedIndices.Count <= 0)
			{
				return;
			}

			int selectedIndex = ListViewMissing.SelectedIndices[0];
			ListViewItem currentItem = ListViewMissing.SelectedItems[0];
			ChangeCurrentForm form = new ChangeCurrentForm(currentItem.SubItems[0].Text, currentItem.SubItems[1].Text, "");
			DialogResult result = form.ShowDialog(this);

			switch (result)
			{
				case DialogResult.Cancel:
					break;

				case DialogResult.OK:
					slave.Add(currentItem.SubItems[0].Text, form.newValue);
					refreshStatus();
					refreshTables();
					break;
			}
		}


		/// <summary>
		/// This is called when the 'Reload' button is pressed. When this happens, reload data from the language files from scratch.
		/// </summary>
		private void ButtonReload_Click(Object sender, EventArgs e)
		{
			DialogResult initialConfirmation = MessageBox.Show(
				null,
				"This will reload the language files from disk.\nAll your existing changes will be lost!\n\nAre you sure?",
				"Language Manager",
				MessageBoxButtons.OKCancel,
				MessageBoxIcon.Warning,
				MessageBoxDefaultButton.Button2);

			if (initialConfirmation == DialogResult.Cancel)
			{
				return;
			}

			if (!File.Exists(TextBoxMaster.Text))
			{
				MessageBox.Show(
					null,
					"The base language file does not exist, based on the path you've supplied.",
					"Language Manager",
					MessageBoxButtons.OK,
					MessageBoxIcon.Error);
				return;
			}

			if (!File.Exists(TextBoxSlave.Text))
			{
				MessageBox.Show(
					null,
					"The subject language file does not exist, based on the path you've supplied.",
					"Language Manager",
					MessageBoxButtons.OK,
					MessageBoxIcon.Error);
				return;
			}

			loadData(TextBoxMaster.Text, ref master);
			loadData(TextBoxSlave.Text, ref slave);

			refreshStatus();
			refreshTables();
		}


		/// <summary>
		/// This is called when the 'Write Changes' button is pressed.
		/// Here, the final resulting slave language file should be written.
		/// </summary>
		private void ButtonApply_Click(Object sender, EventArgs e)
		{
			if(TextBoxMaster.Text.Trim().Length <= 0 || TextBoxSlave.Text.Trim().Length <= 0)
			{
				MessageBox.Show(
					null,
					"Please enter a valid path in the fields at the top of the window.",
					"Language Manager",
					MessageBoxButtons.OK,
					MessageBoxIcon.Error);

				return;
			}

			DialogResult confirmation = MessageBox.Show(
				null,
				"The modified .lang file will be written to the file path currently in the 'Subject' field at the top of the window.\nAny old file that exists will be backed up under a different name.\n\nDo you wish to continue?",
				"Language Manager",
				MessageBoxButtons.OKCancel,
				MessageBoxIcon.Warning,
				MessageBoxDefaultButton.Button2);

			if (confirmation == DialogResult.Cancel)
			{
				return;
			}

			// Attempt to back up any previous file.
			String writePath = TextBoxSlave.Text.Trim();
			if (File.Exists(writePath))
			{
				int counter = 0;

				while (File.Exists(writePath + ".old." + counter))
				{
					counter++;
				}

				File.Copy(writePath, writePath + ".old." + counter);
			}

			// Compile the final result.
			List<String> result = new List<String>();
			result.Add("# Exported from Zora no Densha's Language Manager");

			String value;
			foreach (String key in slave.Keys)
			{
				if (!slave.TryGetValue(key, out value))
				{
					continue;
				}

				if (slaveRedundant.ContainsKey(key))
				{
					continue;
				}

				result.Add(String.Format("{0}={1}", key, value));
			}

			// Optionally include redundant translations.
			if (CheckBoxRedundant.Checked)
			{
				result.Add(("# Redundant Values"));
				foreach (String key in slaveRedundant.Keys)
				{
					if (!slaveRedundant.TryGetValue(key, out value))
					{
						continue;
					}

					result.Add(String.Format("{0}={1}", key, value));
				}
			}
			
			try
			{
				File.WriteAllLines(writePath, result);

				MessageBox.Show(
				null,
				"File written without error.",
				"Language Manager",
				MessageBoxButtons.OK,
				MessageBoxIcon.Information);
			}
			catch(Exception ex)
			{
				MessageBox.Show(
					null,
					"Couldn't write the language file.\n" + ex.Message,
					"Language Manager",
					MessageBoxButtons.OK,
					MessageBoxIcon.Error);
			}
		}
	}
}
