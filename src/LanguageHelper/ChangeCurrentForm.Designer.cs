﻿
namespace LanguageHelper
{
	partial class ChangeCurrentForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ButtonApply = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.LabelElement = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.ButtonCancel = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.TextBoxTranslation = new System.Windows.Forms.TextBox();
			this.LabelReference = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// ButtonApply
			// 
			this.ButtonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ButtonApply.Location = new System.Drawing.Point(416, 205);
			this.ButtonApply.Name = "ButtonApply";
			this.ButtonApply.Size = new System.Drawing.Size(115, 23);
			this.ButtonApply.TabIndex = 1;
			this.ButtonApply.Text = "Apply";
			this.ButtonApply.UseVisualStyleBackColor = true;
			this.ButtonApply.Click += new System.EventHandler(this.ButtonApply_Click);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(12, 56);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(91, 69);
			this.label4.TabIndex = 8;
			this.label4.Text = "Reference";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// LabelElement
			// 
			this.LabelElement.BackColor = System.Drawing.Color.White;
			this.LabelElement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.LabelElement.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.LabelElement.Location = new System.Drawing.Point(109, 9);
			this.LabelElement.Name = "LabelElement";
			this.LabelElement.Size = new System.Drawing.Size(422, 46);
			this.LabelElement.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(91, 46);
			this.label3.TabIndex = 6;
			this.label3.Text = "Element";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ButtonCancel
			// 
			this.ButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ButtonCancel.Location = new System.Drawing.Point(335, 205);
			this.ButtonCancel.Name = "ButtonCancel";
			this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
			this.ButtonCancel.TabIndex = 2;
			this.ButtonCancel.Text = "Cancel";
			this.ButtonCancel.UseVisualStyleBackColor = true;
			this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 126);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(91, 69);
			this.label2.TabIndex = 11;
			this.label2.Text = "Translation";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// TextBoxTranslation
			// 
			this.TextBoxTranslation.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.TextBoxTranslation.Location = new System.Drawing.Point(109, 126);
			this.TextBoxTranslation.MaxLength = 250;
			this.TextBoxTranslation.Multiline = true;
			this.TextBoxTranslation.Name = "TextBoxTranslation";
			this.TextBoxTranslation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.TextBoxTranslation.Size = new System.Drawing.Size(422, 69);
			this.TextBoxTranslation.TabIndex = 0;
			// 
			// LabelReference
			// 
			this.LabelReference.BackColor = System.Drawing.Color.White;
			this.LabelReference.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.LabelReference.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.LabelReference.Location = new System.Drawing.Point(109, 56);
			this.LabelReference.Name = "LabelReference";
			this.LabelReference.Size = new System.Drawing.Size(422, 69);
			this.LabelReference.TabIndex = 12;
			// 
			// ChangeCurrentForm
			// 
			this.AcceptButton = this.ButtonApply;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.ButtonCancel;
			this.ClientSize = new System.Drawing.Size(543, 240);
			this.Controls.Add(this.LabelReference);
			this.Controls.Add(this.TextBoxTranslation);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.ButtonCancel);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.LabelElement);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.ButtonApply);
			this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ChangeCurrentForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Edit Translation";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button ButtonApply;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label LabelElement;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button ButtonCancel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox TextBoxTranslation;
		private System.Windows.Forms.Label LabelReference;
	}
}