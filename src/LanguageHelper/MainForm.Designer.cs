﻿
namespace LanguageHelper
{
	partial class MainForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PanelTop = new System.Windows.Forms.Panel();
			this.ButtonChooseSlave = new System.Windows.Forms.Button();
			this.ButtonChooseMaster = new System.Windows.Forms.Button();
			this.LabelSlave = new System.Windows.Forms.Label();
			this.LabelMaster = new System.Windows.Forms.Label();
			this.TextBoxSlave = new System.Windows.Forms.TextBox();
			this.TextBoxMaster = new System.Windows.Forms.TextBox();
			this.GroupBoxMaster = new System.Windows.Forms.GroupBox();
			this.LabelMasterElementsCount = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.ButtonRefresh = new System.Windows.Forms.Button();
			this.TabControl = new System.Windows.Forms.TabControl();
			this.TapPageCurrent = new System.Windows.Forms.TabPage();
			this.ListViewCurrent = new System.Windows.Forms.ListView();
			this.ColumnCurrentKey = new System.Windows.Forms.ColumnHeader();
			this.ColumnCurrentReference = new System.Windows.Forms.ColumnHeader();
			this.ColumnCurrentValue = new System.Windows.Forms.ColumnHeader();
			this.TabPageMissing = new System.Windows.Forms.TabPage();
			this.ListViewMissing = new System.Windows.Forms.ListView();
			this.ColumnMissingKey = new System.Windows.Forms.ColumnHeader();
			this.ColumnMissingReference = new System.Windows.Forms.ColumnHeader();
			this.TabPageRedundant = new System.Windows.Forms.TabPage();
			this.ListViewRedundant = new System.Windows.Forms.ListView();
			this.ColumnRedundantKey = new System.Windows.Forms.ColumnHeader();
			this.ColumnRedudantValue = new System.Windows.Forms.ColumnHeader();
			this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.GroupBoxSlave = new System.Windows.Forms.GroupBox();
			this.LabelSlaveRedundantCount = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.LabelSlaveMissingCount = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.LabelSlaveElementsCount = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.ButtonReload = new System.Windows.Forms.Button();
			this.ButtonApply = new System.Windows.Forms.Button();
			this.CheckBoxRedundant = new System.Windows.Forms.CheckBox();
			this.GroupBoxTasks = new System.Windows.Forms.GroupBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.ProgressBar = new System.Windows.Forms.ProgressBar();
			this.PanelTop.SuspendLayout();
			this.GroupBoxMaster.SuspendLayout();
			this.TabControl.SuspendLayout();
			this.TapPageCurrent.SuspendLayout();
			this.TabPageMissing.SuspendLayout();
			this.TabPageRedundant.SuspendLayout();
			this.GroupBoxSlave.SuspendLayout();
			this.GroupBoxTasks.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// PanelTop
			// 
			this.PanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PanelTop.Controls.Add(this.ButtonChooseSlave);
			this.PanelTop.Controls.Add(this.ButtonChooseMaster);
			this.PanelTop.Controls.Add(this.LabelSlave);
			this.PanelTop.Controls.Add(this.LabelMaster);
			this.PanelTop.Controls.Add(this.TextBoxSlave);
			this.PanelTop.Controls.Add(this.TextBoxMaster);
			this.PanelTop.Location = new System.Drawing.Point(325, 16);
			this.PanelTop.Name = "PanelTop";
			this.PanelTop.Size = new System.Drawing.Size(581, 56);
			this.PanelTop.TabIndex = 2;
			// 
			// ButtonChooseSlave
			// 
			this.ButtonChooseSlave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ButtonChooseSlave.Location = new System.Drawing.Point(447, 30);
			this.ButtonChooseSlave.Name = "ButtonChooseSlave";
			this.ButtonChooseSlave.Size = new System.Drawing.Size(131, 24);
			this.ButtonChooseSlave.TabIndex = 5;
			this.ButtonChooseSlave.Text = "Choose";
			this.ButtonChooseSlave.UseVisualStyleBackColor = true;
			this.ButtonChooseSlave.Click += new System.EventHandler(this.ButtonChooseSlave_Click);
			// 
			// ButtonChooseMaster
			// 
			this.ButtonChooseMaster.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ButtonChooseMaster.Location = new System.Drawing.Point(447, 2);
			this.ButtonChooseMaster.Name = "ButtonChooseMaster";
			this.ButtonChooseMaster.Size = new System.Drawing.Size(131, 24);
			this.ButtonChooseMaster.TabIndex = 4;
			this.ButtonChooseMaster.Text = "Choose";
			this.ButtonChooseMaster.UseVisualStyleBackColor = true;
			this.ButtonChooseMaster.Click += new System.EventHandler(this.ButtonChooseMaster_Click);
			// 
			// LabelSlave
			// 
			this.LabelSlave.Location = new System.Drawing.Point(3, 34);
			this.LabelSlave.Name = "LabelSlave";
			this.LabelSlave.Size = new System.Drawing.Size(58, 19);
			this.LabelSlave.TabIndex = 3;
			this.LabelSlave.Text = "Subject";
			// 
			// LabelMaster
			// 
			this.LabelMaster.Location = new System.Drawing.Point(3, 6);
			this.LabelMaster.Name = "LabelMaster";
			this.LabelMaster.Size = new System.Drawing.Size(58, 19);
			this.LabelMaster.TabIndex = 2;
			this.LabelMaster.Text = "Base";
			// 
			// TextBoxSlave
			// 
			this.TextBoxSlave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TextBoxSlave.Location = new System.Drawing.Point(67, 31);
			this.TextBoxSlave.Name = "TextBoxSlave";
			this.TextBoxSlave.Size = new System.Drawing.Size(374, 22);
			this.TextBoxSlave.TabIndex = 1;
			// 
			// TextBoxMaster
			// 
			this.TextBoxMaster.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TextBoxMaster.Location = new System.Drawing.Point(67, 3);
			this.TextBoxMaster.Name = "TextBoxMaster";
			this.TextBoxMaster.Size = new System.Drawing.Size(374, 22);
			this.TextBoxMaster.TabIndex = 0;
			// 
			// GroupBoxMaster
			// 
			this.GroupBoxMaster.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.GroupBoxMaster.Controls.Add(this.LabelMasterElementsCount);
			this.GroupBoxMaster.Controls.Add(this.label1);
			this.GroupBoxMaster.Location = new System.Drawing.Point(642, 82);
			this.GroupBoxMaster.Name = "GroupBoxMaster";
			this.GroupBoxMaster.Size = new System.Drawing.Size(264, 81);
			this.GroupBoxMaster.TabIndex = 3;
			this.GroupBoxMaster.TabStop = false;
			this.GroupBoxMaster.Text = "Base File Status";
			// 
			// LabelMasterElementsCount
			// 
			this.LabelMasterElementsCount.BackColor = System.Drawing.Color.White;
			this.LabelMasterElementsCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.LabelMasterElementsCount.Location = new System.Drawing.Point(103, 18);
			this.LabelMasterElementsCount.Name = "LabelMasterElementsCount";
			this.LabelMasterElementsCount.Size = new System.Drawing.Size(155, 23);
			this.LabelMasterElementsCount.TabIndex = 1;
			this.LabelMasterElementsCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(6, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(91, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Elements";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ButtonRefresh
			// 
			this.ButtonRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ButtonRefresh.Location = new System.Drawing.Point(135, 81);
			this.ButtonRefresh.Name = "ButtonRefresh";
			this.ButtonRefresh.Size = new System.Drawing.Size(123, 23);
			this.ButtonRefresh.TabIndex = 0;
			this.ButtonRefresh.Text = "Refresh";
			this.ButtonRefresh.UseVisualStyleBackColor = true;
			this.ButtonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
			// 
			// TabControl
			// 
			this.TabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TabControl.Controls.Add(this.TapPageCurrent);
			this.TabControl.Controls.Add(this.TabPageMissing);
			this.TabControl.Controls.Add(this.TabPageRedundant);
			this.TabControl.Location = new System.Drawing.Point(12, 82);
			this.TabControl.Name = "TabControl";
			this.TabControl.SelectedIndex = 0;
			this.TabControl.Size = new System.Drawing.Size(624, 564);
			this.TabControl.TabIndex = 4;
			// 
			// TapPageCurrent
			// 
			this.TapPageCurrent.Controls.Add(this.ListViewCurrent);
			this.TapPageCurrent.Location = new System.Drawing.Point(4, 23);
			this.TapPageCurrent.Name = "TapPageCurrent";
			this.TapPageCurrent.Padding = new System.Windows.Forms.Padding(3);
			this.TapPageCurrent.Size = new System.Drawing.Size(616, 537);
			this.TapPageCurrent.TabIndex = 0;
			this.TapPageCurrent.Text = "Current";
			this.TapPageCurrent.UseVisualStyleBackColor = true;
			// 
			// ListViewCurrent
			// 
			this.ListViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ListViewCurrent.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ListViewCurrent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnCurrentKey,
            this.ColumnCurrentReference,
            this.ColumnCurrentValue});
			this.ListViewCurrent.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.ListViewCurrent.FullRowSelect = true;
			this.ListViewCurrent.GridLines = true;
			this.ListViewCurrent.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.ListViewCurrent.HideSelection = false;
			this.ListViewCurrent.Location = new System.Drawing.Point(6, 6);
			this.ListViewCurrent.MultiSelect = false;
			this.ListViewCurrent.Name = "ListViewCurrent";
			this.ListViewCurrent.ShowGroups = false;
			this.ListViewCurrent.Size = new System.Drawing.Size(604, 525);
			this.ListViewCurrent.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.ListViewCurrent.TabIndex = 0;
			this.ListViewCurrent.UseCompatibleStateImageBehavior = false;
			this.ListViewCurrent.View = System.Windows.Forms.View.Details;
			this.ListViewCurrent.DoubleClick += new System.EventHandler(this.ListViewCurrent_DoubleClick);
			// 
			// ColumnCurrentKey
			// 
			this.ColumnCurrentKey.Text = "Key";
			this.ColumnCurrentKey.Width = 400;
			// 
			// ColumnCurrentReference
			// 
			this.ColumnCurrentReference.Text = "Reference";
			this.ColumnCurrentReference.Width = 200;
			// 
			// ColumnCurrentValue
			// 
			this.ColumnCurrentValue.Text = "Value";
			this.ColumnCurrentValue.Width = 200;
			// 
			// TabPageMissing
			// 
			this.TabPageMissing.Controls.Add(this.ListViewMissing);
			this.TabPageMissing.Location = new System.Drawing.Point(4, 23);
			this.TabPageMissing.Name = "TabPageMissing";
			this.TabPageMissing.Padding = new System.Windows.Forms.Padding(3);
			this.TabPageMissing.Size = new System.Drawing.Size(616, 537);
			this.TabPageMissing.TabIndex = 1;
			this.TabPageMissing.Text = "Missing";
			this.TabPageMissing.UseVisualStyleBackColor = true;
			// 
			// ListViewMissing
			// 
			this.ListViewMissing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ListViewMissing.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ListViewMissing.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnMissingKey,
            this.ColumnMissingReference});
			this.ListViewMissing.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.ListViewMissing.FullRowSelect = true;
			this.ListViewMissing.GridLines = true;
			this.ListViewMissing.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.ListViewMissing.HideSelection = false;
			this.ListViewMissing.Location = new System.Drawing.Point(6, 6);
			this.ListViewMissing.MultiSelect = false;
			this.ListViewMissing.Name = "ListViewMissing";
			this.ListViewMissing.ShowGroups = false;
			this.ListViewMissing.Size = new System.Drawing.Size(604, 525);
			this.ListViewMissing.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.ListViewMissing.TabIndex = 1;
			this.ListViewMissing.UseCompatibleStateImageBehavior = false;
			this.ListViewMissing.View = System.Windows.Forms.View.Details;
			this.ListViewMissing.DoubleClick += new System.EventHandler(this.ListViewMissing_DoubleClick);
			// 
			// ColumnMissingKey
			// 
			this.ColumnMissingKey.Text = "Key";
			this.ColumnMissingKey.Width = 400;
			// 
			// ColumnMissingReference
			// 
			this.ColumnMissingReference.Text = "Reference";
			this.ColumnMissingReference.Width = 300;
			// 
			// TabPageRedundant
			// 
			this.TabPageRedundant.Controls.Add(this.ListViewRedundant);
			this.TabPageRedundant.Location = new System.Drawing.Point(4, 23);
			this.TabPageRedundant.Name = "TabPageRedundant";
			this.TabPageRedundant.Size = new System.Drawing.Size(616, 537);
			this.TabPageRedundant.TabIndex = 2;
			this.TabPageRedundant.Text = "Redundant";
			this.TabPageRedundant.UseVisualStyleBackColor = true;
			// 
			// ListViewRedundant
			// 
			this.ListViewRedundant.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ListViewRedundant.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ListViewRedundant.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnRedundantKey,
            this.ColumnRedudantValue});
			this.ListViewRedundant.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.ListViewRedundant.FullRowSelect = true;
			this.ListViewRedundant.GridLines = true;
			this.ListViewRedundant.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.ListViewRedundant.HideSelection = false;
			this.ListViewRedundant.Location = new System.Drawing.Point(6, 6);
			this.ListViewRedundant.MultiSelect = false;
			this.ListViewRedundant.Name = "ListViewRedundant";
			this.ListViewRedundant.ShowGroups = false;
			this.ListViewRedundant.Size = new System.Drawing.Size(607, 528);
			this.ListViewRedundant.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.ListViewRedundant.TabIndex = 1;
			this.ListViewRedundant.UseCompatibleStateImageBehavior = false;
			this.ListViewRedundant.View = System.Windows.Forms.View.Details;
			// 
			// ColumnRedundantKey
			// 
			this.ColumnRedundantKey.Text = "Key";
			this.ColumnRedundantKey.Width = 400;
			// 
			// ColumnRedudantValue
			// 
			this.ColumnRedudantValue.Text = "Value";
			this.ColumnRedudantValue.Width = 200;
			// 
			// OpenFileDialog
			// 
			this.OpenFileDialog.AddExtension = false;
			this.OpenFileDialog.DefaultExt = "lang";
			this.OpenFileDialog.FileName = "en_US.lang";
			this.OpenFileDialog.Filter = "Language files|*.lang|All Files|*.*";
			// 
			// GroupBoxSlave
			// 
			this.GroupBoxSlave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.GroupBoxSlave.Controls.Add(this.ProgressBar);
			this.GroupBoxSlave.Controls.Add(this.label5);
			this.GroupBoxSlave.Controls.Add(this.LabelSlaveRedundantCount);
			this.GroupBoxSlave.Controls.Add(this.label6);
			this.GroupBoxSlave.Controls.Add(this.LabelSlaveMissingCount);
			this.GroupBoxSlave.Controls.Add(this.label4);
			this.GroupBoxSlave.Controls.Add(this.LabelSlaveElementsCount);
			this.GroupBoxSlave.Controls.Add(this.label3);
			this.GroupBoxSlave.Location = new System.Drawing.Point(642, 169);
			this.GroupBoxSlave.Name = "GroupBoxSlave";
			this.GroupBoxSlave.Size = new System.Drawing.Size(264, 126);
			this.GroupBoxSlave.TabIndex = 4;
			this.GroupBoxSlave.TabStop = false;
			this.GroupBoxSlave.Text = "Subject File Status";
			// 
			// LabelSlaveRedundantCount
			// 
			this.LabelSlaveRedundantCount.BackColor = System.Drawing.Color.White;
			this.LabelSlaveRedundantCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.LabelSlaveRedundantCount.Location = new System.Drawing.Point(103, 66);
			this.LabelSlaveRedundantCount.Name = "LabelSlaveRedundantCount";
			this.LabelSlaveRedundantCount.Size = new System.Drawing.Size(155, 23);
			this.LabelSlaveRedundantCount.TabIndex = 7;
			this.LabelSlaveRedundantCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(6, 66);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(91, 23);
			this.label6.TabIndex = 6;
			this.label6.Text = "(Redundant)";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// LabelSlaveMissingCount
			// 
			this.LabelSlaveMissingCount.BackColor = System.Drawing.Color.White;
			this.LabelSlaveMissingCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.LabelSlaveMissingCount.Location = new System.Drawing.Point(103, 42);
			this.LabelSlaveMissingCount.Name = "LabelSlaveMissingCount";
			this.LabelSlaveMissingCount.Size = new System.Drawing.Size(155, 23);
			this.LabelSlaveMissingCount.TabIndex = 5;
			this.LabelSlaveMissingCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(6, 42);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(91, 23);
			this.label4.TabIndex = 4;
			this.label4.Text = "(Missing)";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// LabelSlaveElementsCount
			// 
			this.LabelSlaveElementsCount.BackColor = System.Drawing.Color.White;
			this.LabelSlaveElementsCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.LabelSlaveElementsCount.Location = new System.Drawing.Point(103, 18);
			this.LabelSlaveElementsCount.Name = "LabelSlaveElementsCount";
			this.LabelSlaveElementsCount.Size = new System.Drawing.Size(155, 23);
			this.LabelSlaveElementsCount.TabIndex = 3;
			this.LabelSlaveElementsCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(6, 18);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(91, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = "Elements";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ButtonReload
			// 
			this.ButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ButtonReload.Location = new System.Drawing.Point(6, 81);
			this.ButtonReload.Name = "ButtonReload";
			this.ButtonReload.Size = new System.Drawing.Size(123, 23);
			this.ButtonReload.TabIndex = 5;
			this.ButtonReload.Text = "Reload";
			this.ButtonReload.UseVisualStyleBackColor = true;
			this.ButtonReload.Click += new System.EventHandler(this.ButtonReload_Click);
			// 
			// ButtonApply
			// 
			this.ButtonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ButtonApply.Location = new System.Drawing.Point(6, 52);
			this.ButtonApply.Name = "ButtonApply";
			this.ButtonApply.Size = new System.Drawing.Size(252, 23);
			this.ButtonApply.TabIndex = 6;
			this.ButtonApply.Text = "Write Changes";
			this.ButtonApply.UseVisualStyleBackColor = true;
			this.ButtonApply.Click += new System.EventHandler(this.ButtonApply_Click);
			// 
			// CheckBoxRedundant
			// 
			this.CheckBoxRedundant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CheckBoxRedundant.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.CheckBoxRedundant.Checked = true;
			this.CheckBoxRedundant.CheckState = System.Windows.Forms.CheckState.Checked;
			this.CheckBoxRedundant.Location = new System.Drawing.Point(6, 16);
			this.CheckBoxRedundant.Name = "CheckBoxRedundant";
			this.CheckBoxRedundant.Size = new System.Drawing.Size(252, 30);
			this.CheckBoxRedundant.TabIndex = 7;
			this.CheckBoxRedundant.Text = "Also Write Redundant Translations";
			this.CheckBoxRedundant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.CheckBoxRedundant.UseVisualStyleBackColor = true;
			// 
			// GroupBoxTasks
			// 
			this.GroupBoxTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.GroupBoxTasks.Controls.Add(this.CheckBoxRedundant);
			this.GroupBoxTasks.Controls.Add(this.ButtonApply);
			this.GroupBoxTasks.Controls.Add(this.ButtonReload);
			this.GroupBoxTasks.Controls.Add(this.ButtonRefresh);
			this.GroupBoxTasks.Location = new System.Drawing.Point(642, 536);
			this.GroupBoxTasks.Name = "GroupBoxTasks";
			this.GroupBoxTasks.Size = new System.Drawing.Size(264, 110);
			this.GroupBoxTasks.TabIndex = 7;
			this.GroupBoxTasks.TabStop = false;
			this.GroupBoxTasks.Text = "Tasks";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::LanguageHelper.Properties.Resources.logo;
			this.pictureBox1.Location = new System.Drawing.Point(12, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(64, 64);
			this.pictureBox1.TabIndex = 8;
			this.pictureBox1.TabStop = false;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.label2.Location = new System.Drawing.Point(82, 12);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(237, 64);
			this.label2.TabIndex = 9;
			this.label2.Text = "Zora no Densha";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(6, 89);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(91, 23);
			this.label5.TabIndex = 8;
			this.label5.Text = "Progress";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ProgressBar
			// 
			this.ProgressBar.Location = new System.Drawing.Point(103, 92);
			this.ProgressBar.Name = "ProgressBar";
			this.ProgressBar.Size = new System.Drawing.Size(155, 23);
			this.ProgressBar.TabIndex = 9;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(918, 658);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.GroupBoxTasks);
			this.Controls.Add(this.GroupBoxSlave);
			this.Controls.Add(this.TabControl);
			this.Controls.Add(this.GroupBoxMaster);
			this.Controls.Add(this.PanelTop);
			this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.MinimumSize = new System.Drawing.Size(800, 500);
			this.Name = "MainForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Language Manager";
			this.PanelTop.ResumeLayout(false);
			this.PanelTop.PerformLayout();
			this.GroupBoxMaster.ResumeLayout(false);
			this.TabControl.ResumeLayout(false);
			this.TapPageCurrent.ResumeLayout(false);
			this.TabPageMissing.ResumeLayout(false);
			this.TabPageRedundant.ResumeLayout(false);
			this.GroupBoxSlave.ResumeLayout(false);
			this.GroupBoxTasks.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Panel PanelTop;
		private System.Windows.Forms.Button ButtonChooseSlave;
		private System.Windows.Forms.Button ButtonChooseMaster;
		private System.Windows.Forms.Label LabelSlave;
		private System.Windows.Forms.Label LabelMaster;
		private System.Windows.Forms.TextBox TextBoxSlave;
		private System.Windows.Forms.TextBox TextBoxMaster;
		private System.Windows.Forms.GroupBox GroupBoxMaster;
		private System.Windows.Forms.Button ButtonRefresh;
		private System.Windows.Forms.TabControl TabControl;
		private System.Windows.Forms.TabPage TapPageCurrent;
		private System.Windows.Forms.TabPage TabPageMissing;
		private System.Windows.Forms.TabPage TabPageRedundant;
		private System.Windows.Forms.OpenFileDialog OpenFileDialog;
		private System.Windows.Forms.GroupBox GroupBoxSlave;
		private System.Windows.Forms.Label LabelMasterElementsCount;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label LabelSlaveRedundantCount;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label LabelSlaveMissingCount;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label LabelSlaveElementsCount;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ListView ListViewCurrent;
		private System.Windows.Forms.ColumnHeader ColumnCurrentKey;
		private System.Windows.Forms.ColumnHeader ColumnCurrentValue;
		private System.Windows.Forms.ColumnHeader ColumnCurrentReference;
		private System.Windows.Forms.ListView ListViewMissing;
		private System.Windows.Forms.ColumnHeader ColumnMissingKey;
		private System.Windows.Forms.ColumnHeader ColumnMissingReference;
		private System.Windows.Forms.ListView ListViewRedundant;
		private System.Windows.Forms.ColumnHeader ColumnRedundantKey;
		private System.Windows.Forms.ColumnHeader ColumnRedudantValue;
		private System.Windows.Forms.Button ButtonReload;
		private System.Windows.Forms.Button ButtonApply;
		private System.Windows.Forms.CheckBox CheckBoxRedundant;
		private System.Windows.Forms.GroupBox GroupBoxTasks;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ProgressBar ProgressBar;
	}
}

