﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LanguageHelper
{
	public partial class ChangeCurrentForm : Form
	{
		public String newValue;

		public ChangeCurrentForm(String key, String reference, String oldValue)
		{
			InitializeComponent();

			newValue = oldValue;

			LabelElement.Text = key;
			LabelReference.Text = reference;
			TextBoxTranslation.Text = newValue;
		}

		private void ButtonCancel_Click(Object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			Close();
		}

		private void ButtonApply_Click(Object sender, EventArgs e)
		{
			this.newValue = TextBoxTranslation.Text;
			this.DialogResult = DialogResult.OK;
			Close();
		}
	}
}
