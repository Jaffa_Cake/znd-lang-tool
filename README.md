# Zora no Densha Language Tool



This is a tool designed to help translators for Zora no Densha easily translate the mod into their own language and to keep their translations up to date with any changes that might occur with the base language files.
